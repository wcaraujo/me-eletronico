﻿using MercadoEletronico.Domain.Core.Interfaces.Repositorys;
using MercadoEletronico.Domain.Core.Interfaces.Services;
using MercadoEletronico.Domain.Models;
using MercadoEletronico.Domain.Service.Services.Base;
using System.Threading.Tasks;

namespace MercadoEletronico.Domain.Service.Services
{
    public class ServicePedido : ServiceBase<Pedido>, IServicePedido
    {
        private readonly IRepositoryPedido _repository;
        public ServicePedido(IRepositoryPedido repository) : base(repository)
        {
            _repository = repository;
        }
        public Task<bool> Exists(string key)
        {
            return _repository.Exists(key);
        }
    }
}

﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using MercadoEletronico.Domain.Models;
using Microsoft.EntityFrameworkCore;



namespace MercadoEletronico.Infra.Data.EFCore.Context
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        //public ApplicationContext(DbContextOptions<ApplicationContext> options)
        // : base(options)
        //{ }
        public ApplicationContext(DbContextOptions options) : base(options)
        { }
    }
}

﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace MercadoEletronico.Application.DTO.DTO
{


    public class ProdutoDTO
    {
        #region property

        [JsonIgnore]
        public int Id { get; set; }
        [Required(ErrorMessage = "Campo descrição é requerido!")]
        public string Descricao { get; set; }
        [JsonPropertyName("precoUnitario")]
        [Required(ErrorMessage = "Campo preço unitário é requerido!")]
        [DataType(DataType.Currency)]
        public decimal PrecoUnt { get; set; }

        [Required(ErrorMessage = "Campo quantidade é requerido!")]  
        public int Qtd { get; set; }
        #endregion
    }
}

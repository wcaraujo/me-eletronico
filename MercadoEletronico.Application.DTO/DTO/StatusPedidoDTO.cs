﻿using MercadoEletronico.Domain.Enums;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace MercadoEletronico.Application.DTO.DTO
{
    public class StatusPedidoDTO
    {
        #region property
        [JsonConverter(typeof(JsonStringEnumConverter))]

        [Required(ErrorMessage = "Campo status é requerido!")]
        public EStatusPedido Status { get; set; }

        [Required(ErrorMessage = "Campo ItensAprovado é requerido!")]
        public int ItensAprovados { get; set; }

        [Required(ErrorMessage = "Campo ValorAprovado é requerido!")]
        [DataType(DataType.Currency)]
        public decimal ValorAprovado { get; set; }

        [Required(ErrorMessage = "Campo pedido é requerido!")]
        public string Pedido { get; set; }
        #endregion
    }
}

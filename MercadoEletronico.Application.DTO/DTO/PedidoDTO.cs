﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;

namespace MercadoEletronico.Application.DTO.DTO
{

    public class PedidoDTO
    {
        #region property
        [JsonPropertyName("pedido")]
        [Required(ErrorMessage = " NPedido é requerido.")]
        public string NPedido { get; set; }


        public ICollection<ProdutoDTO> Itens { get; set; } = new List<ProdutoDTO>();

        [JsonIgnore]
        public int Qtd => Itens.Sum(x => x.Qtd);  // A quantidade total de itens do pedido é composta pela somatória da qtd de cada item.
        [JsonIgnore]
        public decimal Total => Itens.Sum(x => x.PrecoUnt * x.Qtd);  // O valor total do pedido é composto pela somatória do valor calculado de cada item (precoUnitario * qtd).
        #endregion
    }
}

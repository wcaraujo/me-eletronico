﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using Autofac;
using MercadoEletronico.Application.Interfaces;
using MercadoEletronico.Application.Services;
using MercadoEletronico.Domain.Core.Interfaces.Repositorys;
using MercadoEletronico.Domain.Core.Interfaces.Services;
using MercadoEletronico.Domain.Service.Services;
using MercadoEletronico.Infra.CrossCutting.Adapter.Interfaces;
using MercadoEletronico.Infra.CrossCutting.Adapter.Maps;
using MercadoEletronico.Infra.Data.Repository.Repositorys;

namespace MercadoEletronico.Infra.CrossCutting.IOC.Configurations
{
    public class ConfigurationIOC
    {
        public static void Load(ContainerBuilder builder)
        {
            #region Registra IOC

            #region IOC Application
                builder.RegisterType<ApplicationServicePedido>().As<IApplicationServicePedido>();
            #endregion

            #region IOC Services
            builder.RegisterType<ServicePedido>().As<IServicePedido>();
            #endregion

            #region IOC Repositorys SQL
            builder.RegisterType<RepositoryPedido>().As<IRepositoryPedido>();

            #endregion

            #region IOC Mapper
            builder.RegisterType<MapperPedido>().As<IMapperPedido>();
            builder.RegisterType<MapperProduto>().As<IMapperProduto>();
            #endregion

            #endregion

        }
    }
}

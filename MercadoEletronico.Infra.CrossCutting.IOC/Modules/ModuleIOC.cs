﻿using Autofac;
using MercadoEletronico.Infra.CrossCutting.IOC.Configurations;


namespace MercadoEletronico.Infra.CrossCutting.IOC.Modules
{
    public class ModuleIOC : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            #region Carrega IOC
            ConfigurationIOC.Load(builder);
            #endregion
        }
    }
}

﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using MercadoEletronico.Application.DTO.DTO;
using MercadoEletronico.Domain.Models;
using MercadoEletronico.Infra.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace MercadoEletronico.Infra.CrossCutting.Adapter.Maps
{
    public class MapperPedido : IMapperPedido
    {

        #region property
        public readonly List<PedidoDTO> PedidosDTO = new List<PedidoDTO>();
        #endregion


        #region methods
        public List<PedidoDTO> MapperList(IEnumerable<Pedido> pedidos)
        {
            foreach (var item in pedidos)
            {
                PedidoDTO pedidoDTO = new PedidoDTO
                {
                    NPedido = item.NPedido            
                };

                if (item.Itens != null)
                {
                    foreach (var subItem in item.Itens)
                    {
                        pedidoDTO.Itens.Add(new ProdutoDTO() { Descricao = subItem.Descricao, PrecoUnt = subItem.PrecoUnt, Qtd = subItem.Qtd });
                    }
                }
                PedidosDTO.Add(pedidoDTO);
            }
            return PedidosDTO;
        }
        public PedidoDTO MapperToDTO(Pedido pedido)
        {
            PedidoDTO pedidoDTO = null;
            if (pedido != null)
            {
                pedidoDTO = new PedidoDTO()
                {
                    NPedido = pedido.NPedido                  
                };
                if (pedido.Itens != null)
                {
                    foreach (var item in pedido.Itens)
                    {
                        pedidoDTO.Itens.Add(new ProdutoDTO() { Descricao = item.Descricao, PrecoUnt = item.PrecoUnt, Qtd = item.Qtd });
                    }
                }
            }
            return pedidoDTO;
        }
        public Pedido MapperToEntity(PedidoDTO pedidoDTO)
        {
            Pedido pedido = null;
            if (pedidoDTO != null)
            {
                pedido = new Pedido
                {
                    NPedido = pedidoDTO.NPedido            
                };
                if (pedidoDTO.Itens != null)
                {
                    foreach (var item in pedidoDTO.Itens)
                    {
                        pedido.Itens.Add(new Produto() { Descricao = item.Descricao, PrecoUnt = item.PrecoUnt, Qtd = item.Qtd });
                    }
                }
            }
            return pedido;
        }
        #endregion
    }
}

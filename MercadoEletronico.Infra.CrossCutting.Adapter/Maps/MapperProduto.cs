﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using MercadoEletronico.Application.DTO.DTO;
using MercadoEletronico.Domain.Models;
using MercadoEletronico.Infra.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace MercadoEletronico.Infra.CrossCutting.Adapter.Maps
{
    public class MapperProduto : IMapperProduto
    {
        #region property
        public readonly List<ProdutoDTO> ProdutosDTO = new List<ProdutoDTO>();
        #endregion


        #region methods
        public List<ProdutoDTO> MapperList(IEnumerable<Produto> produtos)
        {
            foreach (var item in produtos)
            {
                ProdutoDTO DTO = new ProdutoDTO
                {
                    Id = item.Id,
                    Descricao = item.Descricao,
                    PrecoUnt = item.PrecoUnt,
                    Qtd = item.Qtd
                };
                ProdutosDTO.Add(DTO);
            }
            return ProdutosDTO;
        }
        public ProdutoDTO MapperToDTO(Produto produto)
        {
            ProdutoDTO DTO = null;
            if (produto != null)
            {
                DTO = new ProdutoDTO()
                {
                    Id = produto.Id,
                    Descricao = produto.Descricao,
                    Qtd = produto.Qtd,
                    PrecoUnt = produto.PrecoUnt
                };
            }
            return DTO;
        }
        public Produto MapperToEntity(ProdutoDTO produtoDTO)
        {
            Produto produto = null;
            if (produtoDTO != null)
            {
                produto = new Produto
                {
                    Id = produtoDTO.Id,
                    Descricao = produtoDTO.Descricao,
                    Qtd = produtoDTO.Qtd,
                    PrecoUnt = produtoDTO.PrecoUnt
                };
            }
            return produto;
        }
        #endregion
    }
}

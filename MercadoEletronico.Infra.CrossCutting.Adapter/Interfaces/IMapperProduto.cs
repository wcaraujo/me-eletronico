﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using MercadoEletronico.Application.DTO.DTO;
using MercadoEletronico.Domain.Models;
using System.Collections.Generic;

namespace MercadoEletronico.Infra.CrossCutting.Adapter.Interfaces
{
    public interface IMapperProduto
    {
        #region Mappers
        Produto MapperToEntity(ProdutoDTO produtoDTO);
        List<ProdutoDTO> MapperList(IEnumerable<Produto> produtos);
        ProdutoDTO MapperToDTO(Produto produto);
        #endregion
    }
}

﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using MercadoEletronico.Application.DTO.DTO;
using MercadoEletronico.Domain.Models;
using System.Collections.Generic;

namespace MercadoEletronico.Infra.CrossCutting.Adapter.Interfaces
{
    public interface IMapperPedido
    {
        #region Mappers
        Pedido MapperToEntity(PedidoDTO pedidoDTO);
        List<PedidoDTO> MapperList(IEnumerable<Pedido> pedidos);
        PedidoDTO MapperToDTO(Pedido pedido);
        #endregion
    }
}

# Back-end Challenge

Bem-vindo(a) projeto para desenvolvedores(as) back-end .NET

## :computer: Tecnologias

* Framework 3.1 do .Net Core.
* C#
* RESTful API Backend
* CRUD APIs
* DDD (Domain Driven Design) and IoC
* In-Memory Entity Framework Core 
* Autofac
* Autofac.Extensions.DependencyInjection
* JsonPatch
* Serilog
* Swashbuckle.




### Instalação ###
* [Clone o git clone https://bitbucket.org/wcaraujo/mercadoeletronico.git](https://bitbucket.org/wcaraujo/mercadoeletronico.git) e edite com seu editor favorito. e.g. Visual Studio, Visual Studio Code.

API tem dois endpoints:

###	Endpoint – Pedido

`http://localhost:{porta}/api/pedido` uma API RESTful. (GET, POST, PUT, DELETE)

O conteúdo de um Pedido possui o seguinte payload:

```json
{
  "pedido":"123456",
  "itens": [
  {
    "descricao": "Item A",
    "precoUnitario": 10,
    "qtd": 1
  },
  {
    "descricao": "Item B",
    "precoUnitario": 5,
    "qtd": 2
  }
  ]
}
```


####Endpoint – Status de Pedido

POST em `http://localhost:{porta}/api/status` com o seguinte payload:

```json
{
  "status":"XXX",
  "itensAprovados":0,
  "valorAprovado":0,
  "pedido":"XXX"
}
```

E terá o seguinte response:

```json
{
  "pedido":"123456",
  "status": ["STATUS_1", "STATUS_2", "STATUS_...n"]
}
```

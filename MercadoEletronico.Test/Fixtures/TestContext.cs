﻿using MercadoEletronico.Presentation.API.Pedido;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;
using Autofac.Extensions.DependencyInjection;



namespace MercadoEletronico.Test.Fixtures
{
    public class TestContext
    {
        public HttpClient Client { get; set; }
        private TestServer _server;
        public TestContext()
        {
            SetupClient();
        }
        private void SetupClient()
        {
            _server = new TestServer(new WebHostBuilder()
                .ConfigureServices(ser => { ser.AddAutofac(); })
                          .UseEnvironment("Development")
                .UseStartup<Startup>());
            Client = _server.CreateClient();
        }
    }
}

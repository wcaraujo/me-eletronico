﻿using FluentAssertions;
using MercadoEletronico.Test.Fixtures;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace MercadoEletronico.Test.Scenarios
{
    public class PedidoControllerTest
    {
        private readonly TestContext _testContext;


        public PedidoControllerTest()
        {
            _testContext = new TestContext();
        }

        [Fact]
        public async Task Get_ById_ReturnsBadRequestResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/pedido/1234567-n");
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }



    }
}

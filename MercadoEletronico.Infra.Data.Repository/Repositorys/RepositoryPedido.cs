﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================
using MercadoEletronico.Domain.Core.Interfaces.Repositorys;
using MercadoEletronico.Domain.Models;
using MercadoEletronico.Infra.Data.EFCore.Context;
using MercadoEletronico.Infra.Data.Repository.Repositorys.Base;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MercadoEletronico.Infra.Data.Repository.Repositorys
{
    public class RepositoryPedido : RepositoryBase<Pedido>, IRepositoryPedido
    {
        private readonly ApplicationContext _ctx;
        #region Constructor
        public RepositoryPedido(ApplicationContext ctx) : base(ctx)
        {
            _ctx = ctx;
        }
        #endregion



        #region methods opcionais  
        public async Task<bool> Exists(string key)
        {
            var isExists = await _ctx.Pedidos.FindAsync(key);
            return isExists != null;
        }
        public new async Task<Pedido> FindByKeyAsync(object key)
        {

            var entity = await _ctx.Pedidos
                .Include(i => i.Itens)
                .AsNoTracking()
                .Where(a => a.NPedido == key.ToString())
                .FirstOrDefaultAsync();
            return entity;

        }
        public new async Task<IEnumerable<Pedido>> GetAllAsync()
        {
            return await _ctx.Pedidos
                .Include(a => a.Itens)
                .ToListAsync();
        }
        #endregion
    }
}

using System;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace MercadoEletronico.Presentation.API.Pedido
{
    public class Program
    {
        public static int Main(string[] args)
        {


            Log.Logger = new LoggerConfiguration()
             .MinimumLevel.Debug()
             .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
             .Enrich.FromLogContext()
             .WriteTo.Console()
             .WriteTo.Seq($"https://localhost:44356/")
             .CreateLogger();
            try
            {
                Log.Information("INICIANDO_API");
                CreateHostBuilder(args)
                     .Build()
                     .Run();
                Log.Information("API INICIDADA");
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "API FINALIZADA");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>()
                 //   .UseIISIntegration()
                    .ConfigureLogging((hostingContext, logging) =>
                     {
                         logging.ClearProviders();
                         logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                         logging.AddConsole();
                         logging.AddDebug();
                         logging.AddEventSourceLogger();
                         logging.AddFile(hostingContext.Configuration.GetSection("Logging"));
                     });
                });
    }
}

﻿using MercadoEletronico.Application.DTO.DTO;
using MercadoEletronico.Application.Interfaces;
using MercadoEletronico.Presentation.API.Pedido.Controllers.Base;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MercadoEletronico.Presentation.API.Pedido.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : BaseController
    {
        public PedidoController(IApplicationServicePedido applicationServicePedido) : base(applicationServicePedido)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<PedidoDTO> lista = (List<PedidoDTO>)await _applicationServicePedido.GetAllAsync();
            if (lista.Count == 0)
                return NotFound(new { mensagem = "Ops, nenhum registro cadastrado!" });

            return Ok(lista);
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var pedido = await _applicationServicePedido.FindByKeyAsync(id);

            if (pedido == null)
                return NotFound(new { mensagem = "Ops, nenhum registro localizado!" });

            return Ok(pedido);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PedidoDTO value)
        {
            try
            {
               
                if (value == null)
                    return BadRequest();

                if (!ModelState.IsValid)
                    return BadRequest();

                if (await _applicationServicePedido.Exists(value.NPedido))
                    return BadRequest(new { mensagem = "Ops, registro já cadastrado!" });


                _applicationServicePedido.Create(value);
                return Ok(new { mensagem = "cadastrado com sucesso!" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] PedidoDTO value)
        {
            try
            {
                if (value == null)
                    return BadRequest();



                var pedido = await _applicationServicePedido.FindByKeyAsync(id);

                if (pedido == null)
                    return NotFound();

                pedido.Itens = value.Itens;

                _applicationServicePedido.Update(pedido);
                return Ok(new { mensagem = "atualizado com sucesso!" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Patch(int id, [FromBody] JsonPatchDocument<PedidoDTO> patchJogoDTO)
        {
            try
            {
                if (patchJogoDTO == null)
                    return BadRequest();


                var jogoDTO = await _applicationServicePedido.FindByKeyAsync(id);

                if (jogoDTO == null)
                    return NotFound();


                patchJogoDTO.ApplyTo(jogoDTO);

                _applicationServicePedido.Update(jogoDTO);
                return Ok(new { mensagem = "atualizado parcialmente com sucesso!" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var pedido = await _applicationServicePedido.FindByKeyAsync(id);


                if (pedido == null)
                    return NotFound();


                _applicationServicePedido.Delete(pedido);

                return Ok(new { mensagem = "removido com sucesso!" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

    }
}

﻿using MercadoEletronico.Application.DTO.DTO;
using MercadoEletronico.Application.Interfaces;
using MercadoEletronico.Domain.Enums;
using MercadoEletronico.Presentation.API.Pedido.Controllers.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MercadoEletronico.Presentation.API.Pedido.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : BaseController
    {
        public StatusController(IApplicationServicePedido applicationServicePedido) : base(applicationServicePedido)
        {
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] StatusPedidoDTO value)
        {
            try
            {

                if (value == null)
                    return BadRequest();

                if (!ModelState.IsValid)
                    return BadRequest();



                var pedido = await _applicationServicePedido.FindByKeyAsync(value.Pedido);
                List<EStatusPedido> statusPedido = new List<EStatusPedido>();


                /* 
                  pedido não for localizado no banco de dados.
                  Então(Then) retorne "status": "CODIGO_PEDIDO_INVALIDO"
                */
                if (pedido == null)
                {
                    statusPedido.Add(EStatusPedido.CODIGO_PEDIDO_INVALIDO);
                    return NotFound(new { status = statusPedido.ConvertAll(s => s.ToString()) });
                }


                //  status for igual a REPROVADO
                if (value.Status == EStatusPedido.REPROVADO)
                {
                    /* 
                    pedido for localizado no banco de dados.              
                    Então (Then) retorne "status": "REPROVADO"
                  */
                    statusPedido.Add(EStatusPedido.REPROVADO);
                    return Ok(new { status = statusPedido.ConvertAll(s => s.ToString()) });
                }
                // status for igual a APROVADO.
                else if (value.Status == EStatusPedido.APROVADO)
                {


                    /* 
                        pedido for localizado no banco de dados.                      
                       => itensAprovados for igual a quantidade de itens do pedido.
                       => valorAprovado for igual o valor total do pedido.                   
                        Então (Then) retorne  "status": "APROVADO"
                    */
                    if (pedido.Qtd == value.ItensAprovados && pedido.Total == value.ValorAprovado)
                    {
                        statusPedido.Add(EStatusPedido.APROVADO);
                    }
                    else
                    {

                        /* 
                         pedido for localizado no banco de dados.                       
                        =>  valorAprovado for menor que o valor total do pedido                      
                        =>  Então (Then) retorne  "status": "APROVADO_VALOR_A_MENOR"
                         */
                        if (value.ValorAprovado < pedido.Total)
                        {
                            statusPedido.Add(EStatusPedido.APROVADO_VALOR_A_MENOR);
                        }

                        /* 
                            pedido for localizado no banco de dados.
                           =>  itensAprovados for menor que a quantidade de itens do pedido.
                           =>   Então (Then) retorne "status": "APROVADO_QTD_A_MENOR"
                        */
                        if (value.ItensAprovados < pedido.Qtd)
                        {
                            statusPedido.Add(EStatusPedido.APROVADO_QTD_A_MENOR);
                        }



                        /*                       
                         pedido for localizado no banco de dados.                  
                        => valorAprovado for maior que o valor total do pedido                  
                        => Então (Then) retorne "status": "APROVADO_VALOR_A_MAIOR"
                        */
                        if (value.ValorAprovado > pedido.Total)
                        {
                            statusPedido.Add(EStatusPedido.APROVADO_VALOR_A_MAIOR);
                        }



                        /*                     
                         pedido for localizado no banco de dados.
                      =>  itensAprovados for maior que a quantidade de itens do pedido.                     
                      =>  Então (Then) retorne "status": "APROVADO_QTD_A_MAIOR"                   

                         */
                        if (value.ItensAprovados > pedido.Qtd)
                        {
                            statusPedido.Add(EStatusPedido.APROVADO_QTD_A_MAIOR);
                        }



                    }
                }



                return Ok(new { pedido = pedido.NPedido, status = statusPedido.ConvertAll(s => s.ToString()) });

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"error : {ex.Message}");
            }
        }

    }
}

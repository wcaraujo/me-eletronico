﻿using MercadoEletronico.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MercadoEletronico.Presentation.API.Pedido.Controllers.Base
{


    public class BaseController : ControllerBase
    {
        protected readonly IApplicationServicePedido _applicationServicePedido;

        public BaseController(IApplicationServicePedido applicationServicePedido)
        {
            _applicationServicePedido = applicationServicePedido;
        }
    }
}

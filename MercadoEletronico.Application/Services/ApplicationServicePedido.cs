﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using MercadoEletronico.Application.DTO.DTO;
using MercadoEletronico.Application.Interfaces;
using MercadoEletronico.Domain.Core.Interfaces.Services;
using MercadoEletronico.Infra.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MercadoEletronico.Application.Services
{
    public class ApplicationServicePedido : IApplicationServicePedido
    {
        #region property
        private readonly IServicePedido _service;
        private readonly IMapperPedido _mapper;
        #endregion


        #region Constructor
        public ApplicationServicePedido(IServicePedido service, IMapperPedido mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        #endregion


        #region methods
        public void Create(PedidoDTO pedidoDTO)
        {
            var entity = _mapper.MapperToEntity(pedidoDTO);
            _service.Create(entity);
        }

        public void Delete(PedidoDTO pedidoDTO)
        {
            var entity = _mapper.MapperToEntity(pedidoDTO);
            _service.Delete(entity);
        }

        public void Dispose()
        {
            _service.Dispose();
        }

        public Task<bool> Exists(string key)
        {
            return _service.Exists(key);
        }

        public async Task<PedidoDTO> FindByKeyAsync(object key)
        {
            var entity = await _service.FindByKeyAsync(key);
            return _mapper.MapperToDTO(entity);
        }

        public async Task<IEnumerable<PedidoDTO>> GetAllAsync()
        {
            var entity = await _service.GetAllAsync();
            return _mapper.MapperList(entity);
        }

        public void Update(PedidoDTO pedidoDTO)
        {
            var entity = _mapper.MapperToEntity(pedidoDTO);
            _service.Update(entity);
        }
        #endregion
    }
}

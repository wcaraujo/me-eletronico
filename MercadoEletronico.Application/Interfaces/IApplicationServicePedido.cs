﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using MercadoEletronico.Application.DTO.DTO;
using MercadoEletronico.Domain.Core.Interfaces.Services.Base;
using System.Threading.Tasks;

namespace MercadoEletronico.Application.Interfaces
{
    public interface IApplicationServicePedido : IServiceBase<PedidoDTO>
    {
        #region methods opcionais
        Task<bool> Exists(string key);
        #endregion

    }
}

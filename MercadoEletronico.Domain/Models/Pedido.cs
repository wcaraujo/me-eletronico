﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================


using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MercadoEletronico.Domain.Models
{
    public class Pedido
    {
        #region property
        [Key]
        public string NPedido { get; set; }
        public ICollection<Produto> Itens { get; set; } = new List<Produto>();
        #endregion
    }
}


﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

namespace MercadoEletronico.Domain.Models
{
    public class Produto
    {
        #region property
        public int Id { get; set; }
        public string Descricao { get; set; }
        public decimal PrecoUnt { get; set; }
        public int Qtd { get; set; }
        #endregion
    }
}

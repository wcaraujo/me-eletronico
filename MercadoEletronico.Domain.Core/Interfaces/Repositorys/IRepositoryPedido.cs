﻿// =============================
// Challenge
// Autor:wesley cordeiro de araujo
// email:analistasistemasnet@gmail.com
// 11/2020
// =============================

using MercadoEletronico.Domain.Core.Interfaces.Repositorys.Base;
using MercadoEletronico.Domain.Models;
using System.Threading.Tasks;

namespace MercadoEletronico.Domain.Core.Interfaces.Repositorys
{
    public interface IRepositoryPedido : IRepositoryBase<Pedido>
    {
        Task<bool> Exists(string key);
    }
}

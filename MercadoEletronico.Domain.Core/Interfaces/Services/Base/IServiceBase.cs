﻿using MercadoEletronico.Domain.Core.Interfaces.Repositorys.Base;

namespace MercadoEletronico.Domain.Core.Interfaces.Services.Base
{
    public interface IServiceBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    { }

}

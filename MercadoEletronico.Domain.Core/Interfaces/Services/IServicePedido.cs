﻿using MercadoEletronico.Domain.Core.Interfaces.Services.Base;
using MercadoEletronico.Domain.Models;
using System.Threading.Tasks;

namespace MercadoEletronico.Domain.Core.Interfaces.Services
{
    public interface IServicePedido : IServiceBase<Pedido>
    {
        Task<bool> Exists(string key);

    }
}
